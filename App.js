import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import TodoList from './TodoList';
import connection from './connection'

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>TODO</Text>
        <AppComponent />
      </View>
    );
  }
}

class AppComponent extends Component {
  constructor() {
    super()
    this.state = {
      items: []
    }

    this.handleAddNewTodo = this.handleAddNewTodo.bind(this)
    this.handleCompleteToggle = this.handleCompleteToggle.bind(this)
  }

  componentDidMount() {
    const self = this
    const queryDescriptor = {
      $sort: {
        completed: 1,
        label: 1
      }
    }
    const query = connection.createSubscribeQuery('todos', queryDescriptor)

    const update = () => {
      self.setState({
        ...self.state,
        items: query.results
      })
    }

    query.on('ready', update)
    query.on('changed', update)
  }

  handleAddNewTodo(label) {
    const doc = connection.get('todos', uuid())
    doc.create({
      label,
      completed: false
    })
  }

  handleCompleteToggle(itemId, completed) {
    const op = [{p: ['completed'], od: !completed, oi: completed}]
    connection
      .get('todos', itemId)
      .submitOp(op, (error) => {
        if (error) {
          throw error
        }
      })
  }

  render() {
    return (
      <TodoList
        items={this.state.items}
        handleCompleteToggle={this.handleCompleteToggle}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  title: {
    fontSize: 32,
    fontWeight: '600',
    paddingTop: 32,
    paddingLeft: 10,
    paddingRight: 10
  }
});
