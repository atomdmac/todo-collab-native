import React, { Component } from 'react';
import { FlatList, StyleSheet, Switch, Text, View } from 'react-native';

class TodoItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ...props
    }
  }

  componentDidMount() {
    this.props.item.subscribe()
    this.props.item.on('load', update)
    this.props.item.on('op', update)
    const self = this;
    function update() {
      self.forceUpdate()
    }
  }

  componentWillUnmount() {
    this.props.item.unsubscribe()
  }

  render() {
    const toggleComplete = (event) =>
      this.props.handleCompleteToggle(this.props.item.id, !this.props.item.data.completed)

    const {
      item
    } = this.props

    return (
      <View style={styles.item}>
        <Switch
          value={item.data.completed}
          onValueChange={toggleComplete}
        />
        <Text>{item.data.label}</Text>
      </View>
    )
  }
}

export default class TodoList extends Component {
  render() {
    const getItem = ({item}) => {
      return (
        <View style={styles.item}>
          <Switch value={item.data.completed}/>
          <Text>{item.data.label}</Text>
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.items}
          keyExtractor={item => String(item.id)}
          renderItem={
            ({item}) =>
              <TodoItem
                item={item}
                handleCompleteToggle={this.props.handleCompleteToggle}
              />
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 0
  },
  item: {
    display: 'flex',
    flexDirection: 'row',
    padding: 10,
    fontSize: 18,
    height: 44,
  },
})
